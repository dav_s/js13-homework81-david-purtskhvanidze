const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ShortSchema = new Schema({
    shortUrl: {
        type: String,
        required: true
    },
    originalUrl: {
        type: String,
        required: true
    }
});

const Short = mongoose.model('Short', ShortSchema);

module.exports = Short;