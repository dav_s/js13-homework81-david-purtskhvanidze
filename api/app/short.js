const express = require('express');
const { nanoid } = require('nanoid');
const Short = require("../models/Short");

const router = express.Router();

//----- Эти 2 get запроса ("/","/:id") для фронтенда не нужны, но я их оставил
router.get('/', async (req, res, next) => {
    try {
        const query = {};

        const shorts = await Short.find(query);

        return res.send(shorts);
    } catch (e) {
        next(e);
    }
});
router.get('/:id', async (req, res, next) => {
    try {
        const short = await Short.findOne({shortUrl: req.params.id});

        if (!short) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(short);
    } catch (e) {
        next(e);
    }
});
//-----

router.get('/redirect/:short', async (req, res, next) => {
    try {
        const url = await Short.findOne({shortUrl: req.params.short});

        if (!url) {
            return res.status(404).send({message: 'Not found'});
        }

        res.status(301).redirect(url.originalUrl)
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.originalUrl) {
            return res.status(400).send({message: 'Url is required'});
        }

        const shortData = {
            shortUrl: nanoid(7),
            originalUrl: req.body.originalUrl,
        };

        const short = new Short(shortData);
        await short.save();

        return res.send({shortUrl: shortData.shortUrl});
    } catch (e) {
        next(e);
    }
});

module.exports = router;