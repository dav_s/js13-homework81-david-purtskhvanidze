import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UrlData } from './url.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @ViewChild('f') form!: NgForm;
  title = 'frontend';
  generatedUrl = '';

  constructor(private http: HttpClient) { }

  onSubmit() {
    const urlData: UrlData = this.form.value;

    return this.http.post<UrlData>(environment.apiUrl + '/short', urlData).subscribe((e) => {
      this.generatedUrl = environment.apiUrl + '/short/redirect/' + e.shortUrl;
    });
  }

}
