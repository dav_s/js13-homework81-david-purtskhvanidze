export class Url {
  constructor(
    public id: string,
    public shortUrl: string,
    public originalUrl: string,
  ) {}
}

export interface UrlData {
  shortUrl: string;
  originalUrl: string;
}
